using Nauka1.Klasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1
{
    class Program:ICloneable
    {
        private static int _temp;
        private static int _temp2 = 0;

        static void Main(string[] args)
        {
            var animal = new Animal();
            animal.IsAlive = false;

            //1 if 
            _temp = 2;
            var a = ++_temp2;
            var b = _temp2++;


            object variable = 2;
           
            var c = _temp == _temp2;
            var d = _temp == _temp2 ? "Y" : "N";

            var f = variable == null ? 0 : variable;
            var e = variable ?? 0;

            if (_temp == _temp2)
            {
                d = "Y";
            }
            else
            {
                d = "N";
            }

            if (c)
            {
                d = "B";
            }

            //2 loop
            List<Animal> animals = new List<Animal>();
            List<Animal> animals2 = new List<Animal>();

            Console.WriteLine("FOR");
            for (var i = 0; i<10; i++)
            {
                animals.Add(new Animal()
                {
                    IsAlive = true,
                    Age = i
                });

                Console.WriteLine(i);
            }

            var animalAge = animals[3].Age;

            Console.WriteLine("WHILE");
            while(_temp<15)
            {
                animals.Add(new Animal()
                {
                    IsAlive = true
                });
                _temp++;
            }

            foreach(var x in animals)
            {
                x.Age++;
            }

            for(var j = 0;j<animals.Count;j++)
            {
                animals[j].Age++;
            }

            animals2 = animals;
            animals2.Clear();
            animals2 = animals;
            animals.Clear();
          //  GC.Collect();
            animals2 = animals;
            animals.Clear();

            //3 break, continue, return

            Settings.Password = "true";

            foo();

            //var dog = new Dog();
            // dog.Do();

            Continent continent = new Continent("Europe", "Earth");      
            Planet planet = new Planet();
            continent.Dispose();
            Console.ReadKey();
        }

        private static void foo()
        {
            for(var i =0;i<10;i++)
            {
                if (i<6)
                {
                    continue;
                }
                if (i==7)
                {
                    break;
                }
                if (i==8)
                {
                    return;
                }
            }

            Console.WriteLine("BREAK");
        }


        public object Clone()
        {
            return null;
        }
    }
}
