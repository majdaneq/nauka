﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1.Klasy
{
    public abstract class BaseModel : IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine("element disposed");
        }

        public abstract void SaySomething();
        
    }
}
