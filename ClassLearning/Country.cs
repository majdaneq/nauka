﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1.Klasy
{
    public class Country:BaseModel
    {
        public Country()
        {

        }

        public Country(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public override void SaySomething()
        {
            Console.WriteLine("Im an abstract method from Planet class");
        }
    }
}
