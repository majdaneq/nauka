﻿using Nauka1.Interfaces;
using Nauka1.Klasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1.Classes
{
    public class InterfaceClass : BaseModel, SampleInterface, SampleInterface2
    {
        public string Ask()
        {
            throw new NotImplementedException();
        }

        public void Do()
        {
            throw new NotImplementedException();
        }

        public void Finish()
        {
            throw new NotImplementedException();
        }

        public override void SaySomething()
        {
            throw new NotImplementedException();
        }
    }
}
