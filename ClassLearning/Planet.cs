﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1.Klasy
{
    public class Planet:BaseModel
    {        
        public Planet():this(Const.ConstValues.PlanetEarth)
        {

        }

        public Planet(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public double Area { get; set; }

        public override void SaySomething()
        {
            Console.WriteLine("Im an abstract method from Planet class");
        }
    }
}
