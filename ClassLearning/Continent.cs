﻿using System.Collections.Generic;

namespace Nauka1.Klasy
{
    public class Continent:Planet
    {
        public Continent(string continentName, string PlanetName):base(PlanetName)
        {
            Name = continentName;
        }

        public new string Name { get; set; }

        public Continents ContinentEnum { get; set; }

        public List<Country> Countries { get; set; }

    }
}
