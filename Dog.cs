﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1
{
    public class Dog:Animal
    {
        public Dog()
        {
            IsAlive = true;
            Age = 3;
        }

        public new bool IsAlive { get; set; }

        public override void Die()
        {
            Console.WriteLine("2");
            base.Die();
            Console.WriteLine("3");
        }
    }
}
