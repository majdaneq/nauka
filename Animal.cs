﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1
{
    public class Animal
    {
        public bool IsAlive { get; set; }

        public int Age { get; set; }

        private string Name { get; set; }

        public virtual void Die()
        {
            Console.WriteLine("1");
        }

        public void Die(string x)
        {
            IsAlive = false;
        }
    }
}
