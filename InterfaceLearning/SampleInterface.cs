﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nauka1
{
    public interface SampleInterface
    {
        void Do();

        string Ask();
    }
}
